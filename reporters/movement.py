# Displays all available details about the movement specified in the option.
# No supported formats.
def print_movement(t, show_id=False):
    hidden_meta = ['filename', 'lineno']
    if not show_id:
        hidden_meta.append('movement_id')

    print("{} '{}' '{}'".format(t.date, t.payee, t.narration))
    for m in t.meta:
        if not (m in hidden_meta or m.startswith('__')):
            print("\t[{}={}]".format(m, t.meta[m]))
    print("\n")
    for p in t.postings:
        print("\t{:>10} {}".format(round(p.units.number,2), p.account))
        for m in p.meta:
            if not (m in hidden_meta or m.startswith('__')):
                print("\t\t[{}={}]".format(m, p.meta[m]))
    print("\n")

def movement(movements, options, format):
    if options.get('id') is None:
        print("Missing -o id=MOVEMENTID")
    else:
        for t in movements:
            if t.meta.get('movement_id', None) == options['id']:
                print("\n")
                print_movement(t, True)
