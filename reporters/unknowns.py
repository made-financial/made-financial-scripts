# Reports all movements that need attention as they failed categorization.

from .movement import print_movement

# TODO: exclude anonymous donations from the list of unknonwns,
#       and add option to display them if needed

def unknowns(movements, options, format):
    def has_unknowns(movement):
        # does it have any unknown posting ? then it gets reported as an unknonwn.
        for p in m.postings:
            if p.account.endswith(":Unknown"):
                return True

        # if it does have an unknown entity in the movement...
        if "entity_id" in m.meta and m.meta.get('entity_id') == "unknown":
            # ... do any of the postings have an entity_id which is unknown ?
            one_known = False
            for p in m.postings:
                if 'entity_id' in p.meta:
                    if p.meta.get('entity_id') == 'unknown':
                        return True
                    else:
                        one_known = True

            # ... do we have at least one posting with a known entity_id ?
            if not one_known:
                return True

        return False

    for m in movements:
        if has_unknowns(m):
            print_movement(m, show_id=True)
