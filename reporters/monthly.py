# Reports monthly expenses and income, per account.
# Supported options:
# - from=month : display starting from this month
# - to=month : display until this month
# - month=month : display only this month (from and to are ignored if present)
# - include=account,... : include only the specified accounts in the report
# - exclude=account,... : exclude the specified accounts from the report

import sys
from collections import namedtuple
import pandas as pd
from decimal import Decimal as D
from types import SimpleNamespace as SN
import matplotlib.pyplot as plt

sys.path.append("../data/plugins")
from categorize.entities import Entities

entities = Entities("../data/")

def find_who(movement, post):
    id = post.meta.get('entity_id', movement.meta.get('entity_id', None))
    if id is not None:
        try:
            member = entities.members.get(int(id), None)
        except ValueError:
            member = None

        if member is not None:
            return member['fullname']

        customer = entities.customers.get(id, None)
        return customer['fullname'] if customer else None
    else:
        return movement.payee if movement.payee != '' else None

new_group = lambda: SN(total=D(0.0), count=0)
groups = {}

def add_to_group(p, name):
    group = groups.get(name, new_group())
    group.total += p.units.number
    group.count += 1
    groups[name] = group

def check_group(p):
    for group in groups:
        if (group.endswith(":") and p.account.startswith(group)) \
        or p.account == group:
            add_to_group(p, group)
            return True

def monthly(movements, options, format):
    items = []
    month = options.get('month')
    start = options.get('from')
    end = options.get('to')

    if month is None and start is None:
        print("You must pass either a 'month' option or a 'from' option.")
        return

    exclude = options.get('exclude', '')
    exclude = exclude.split(",") if len(exclude) > 0 else []

    include = options.get('include', '')
    include = include.split(",") if len(include) > 0 else []

    for m in movements:
        # All movmements should have an id, if not discard them for now.
        if not 'movement_id' in m.meta:
            continue

        # See the "member" reporter for an explanation of how we determine the
        # period for a posting.
        global_period = m.meta.get('payment_period',
                                   "{}-{:0>2}".format(m.date.year, m.date.month))
        for p in m.postings:
            period = p.meta.get('payment_period', global_period)

            # We either have a range (with just start or start+end), or we
            # have a single month. Filter based on this criteria.
            if start is not None:
                if period < start:
                    continue
                if end is not None and period > end:
                    continue
            elif period != month:
                continue

            # We are only interested in keeping tracks of income and expenses
            # for the month.
            if not (p.account.startswith("Expenses:") or
                    p.account.startswith("Income:")):
                    continue

            # Exclude or include accounts as requested
            if p.account in exclude:
                continue
            if len(include) > 0 and p.account not in include:
                continue

            # Sum the movement up to its own account group and categorize it
            add_to_group(p, p.account)
            who = find_who(m, p)

            def clear_narration(m, p):
                for n in ["TRASPAS A COMPTE DE:", "TRANSF CTE DE:", "TRANSFERENCIA A:", "R/ "]:
                    if m.narration.startswith(n):
                        if p.account.endswith("Unknown"):
                            return m.narration.replace(n, "")
                        else:
                            return ""
                return m.narration

            item = SN(period = period,
                      id = m.meta['movement_id'],
                      amount = round(p.units.number, 2) * -1,
                      account = p.account,
                      narration = clear_narration(m, p),
                      who = who or "")
            items.append(item)

    line_size = 26
    line = "-" * line_size

    # group and sum the data so that we have a table with the
    # columns [period, account, amount]
    data = pd.DataFrame().from_records([item.__dict__ for item in items])
    data = data.groupby(['period', 'account'], as_index=False)['amount'].sum()

    # Add a "direction" column that we can use to know if something is an
    # expense or an income
    data['direction'] = data['account'].apply(lambda a: a.split(':')[0])

    by_period = data.groupby('period')
    if format is None:

        # Get a list of all the periods included in this report and for each one
        # produce a report
        periods = [p for p in by_period['period'].apply(lambda x: x).drop_duplicates()]
        for period in periods:
            # Select the current period
            period_data = by_period.get_group(period)

            # Header
            print("{}\n{:^26}\n{}".format(line, "Month: {}".format(period), line))

            # Produce the balance per account
            for account, amount in period_data.groupby('account')['amount'].sum().items():
                account = account.replace("Income:", "").replace("Expenses:", "")
                print("{:<15}: {:>9.2f}".format(account, amount))

            # Produce the income and expense total using the "direction" column
            print(line)
            for direction, amount in period_data.groupby('direction')['amount'].sum().items():
                print("{:<15}: {:>9.2f}".format(direction, amount))

            # Produce the final balance for the month
            print("{}\n{:<15}: {:>9.2f}\n{}\n".format(line, "Balance",
                                                      period_data['amount'].sum(), line))

        # Produce a global balance
        print("{:<15}: {:>9.2f}\n".format("Total", data['amount'].sum()))

    elif format == "js":
        months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"]
        periods = by_period['period'].apply(lambda x: x).drop_duplicates()
        period_names = ['"{}"'.format(months[int(p.split("-")[1]) - 1]) for p in periods]
        totals = ["{:>.2f}".format(t) for t in by_period['amount'].sum()]
        print("var all = {")
        print("\tlabels: [{}],".format(",".join(period_names)))
        print("\tseries: [[{}]]".format(",".join(totals)))
        print("};\n")

        print("var expenses = [")
        for i, period in enumerate(periods):
            period_data = by_period.get_group(period)
            labels = []
            series = []
            for account, amount in period_data.groupby('account')['amount'].sum().items():
                if account.startswith("Income:"):
                    continue
                account = account.replace("Expenses:", "")
                labels.append(account)
                series.append(amount * -1)

            print("{}".format("," if i > 0 else "") + "\n{")
            print("\tlabels: [{}],".format(",".join(map(lambda l: '"{}"'.format(l), labels))))
            print("\tseries: [{}]".format(",".join(map(lambda l: '{:>.2f}'.format(l), series))))
            print("}")
        print("];")

    elif format == "csv":
        print("Income and Expense per month")
        print("Period,Income,Expenses,Balance")
        periods = by_period['period'].apply(lambda x: x).drop_duplicates()
        for i, period in enumerate(periods):
            period_data = by_period.get_group(period)
            for direction, amount in period_data.groupby('direction')['amount'].sum().items():
                if direction == "Income":
                    income = amount
                else:
                    expense = amount

            print("{},{},{},{}".format(period, income, expense, period_data['amount'].sum()))

        print("")

        print("Income and Expenses per account")
        print("Account,Amount")
        by_account = data.groupby("account")
        for account, group in by_account:
            print("{},{}".format(account, group['amount'].sum()))
