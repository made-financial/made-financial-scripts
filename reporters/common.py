def get_refunded(movements):
    # gather all the members payments that have been refunded, so we can exclude
    # them from the report
    refunded = set()
    for m in movements:
        if m.meta.get('refunded_by'):
            refunded.add(m.meta.get('movement_id'))
            continue

        for p in m.postings:
            if p.account == "Expenses:Refunds":
                ref = p.meta.get('reference', m.meta.get('reference', None))
                if ref:
                    refunded.add(ref)
    return refunded
