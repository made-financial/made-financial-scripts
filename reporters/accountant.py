import sys
import datetime
from collections import namedtuple
from beancount.core import account as accounts
from decimal import Decimal as D

sys.path.append("../data/plugins")
from categorize.entities import Entities
entities = Entities("../data/")

def quote(s):
    return "\"{0}\"".format(s)

def find_who(movement, post):
    # There are two options here for finding out who paid for something.
    # Either there is an entity_id without any paid_by, which means someome paid
    # for themselves. Or we have an entity_id, which means someone else paid for them.
    # The paid_by overrides the entity_id in this case, as for tax declarations we want to
    # know who to invoice to.
    # Note that the paid_by and entity_id can be either on the post or on the
    # movement (i.e. all posts are for/by the same entity), with the post having
    # priority and the movement being the fallback if the post has nothing.

    id = post.meta.get('paid_by', None)
    if id is None:
        id = movement.meta.get('paid_by', None)

    if id is None:
        id = post.meta.get('entity_id', None)
        if id is None:
            id = movement.meta.get('entity_id', None)

    if id is not None:
        try:
            return entities.members.get(int(id), None)
        except ValueError:
            pass

        try:
            return entities.customers.get(id, None)
        except ValueError:
            pass
    return None

def accountant(movements, options, format):
    try:
        cutoff = datetime.datetime.strptime(options.get('from'), "%Y-%m-%d").date()
    except (ValueError, TypeError) as e:
        print("Invalid or missing 'from' option:", options.get('from'), e)
        return

    try:
        num = int(options.get('last'))
    except (ValueError, TypeError) as e:
        print("Invalid of missing 'last' option:", options.get('last'), e)
        return

    csv = [["Orden", "Mes", "Dia", "Num.Factura", "Cliente", "NIF", "Tipo Operacion", "Base Imponible", "Tipo IVA", "Retencion", "IVA Repercutido", "Total Factura", "Total Cobrar"]]

    type_map = {
        "Membership": "Cuota social",
        "Donations": "Donativo",
        "Laser": "Donativo mantenimiento laser",
        "Services": "Servicios"
    }

    ids = {}

    for m in movements:
        if m.date <= cutoff:
            continue

        for p in m.postings:
            if not p.account.startswith("Income:"):
                continue

            # We only recognize and report certain categories of income for
            # tax purposes. Others, such as refunds, are not really income.
            account = accounts.leaf(p.account)
            if type_map.get(account, None) is None:
                continue

            who = find_who(m, p)
            if who is not None:
                name = who.get('fullname','')
                fiscal = who.get('fiscal_id', '').upper()
            else:
                name = ""
                fiscal = ""

            def next_id(movement_id):
                if ids.get(movement_id) is not None:
                    ids[movement_id] += 1
                    return movement_id + "-" + str(ids.get(movement_id))
                else:
                    ids[movement_id] = 1
                    return movement_id

            def append_with_values(num, date, id, name, fiscal, account,
                                   vat_percent, paid):
                base = (D(paid) / D(100.0 + vat_percent)) * D(100.0)
                vat = D(paid) - base
                csv.append([
                    str(num), str(date.month), str(date.day),
                    id, quote(name), fiscal, type_map.get(account, "__OTHER__"),
                    "{:.2f}".format(base),
                    "{:.0f}%".format(vat_percent), "NO", "{:.2f}".format(vat),
                    "{:.2f}".format(paid), "{:.2f}".format(paid)
                ])

            value = p.units.number * -1

            if p.account == "Income:Membership" and value > 35:
                num += 1
                append_with_values(num, m.date, next_id(m.meta.get("movement_id", "")),
                                   name, fiscal, "Services", 21.0, value - D(35.0))
                value = D(35.0)

            num += 1
            append_with_values(num, m.date, next_id(m.meta.get("movement_id", "")),
                               name, fiscal, account,
                               21.0 if account == "Services" else 0.0, value)

    data = "\n".join([",".join(l) for l in csv])
    print(data)
