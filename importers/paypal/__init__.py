"""
Importer for PayPal data.

We export movement history from paypal from the start of the year to
the current date into paypal-current.csv.
Historical information from previous years is stored as paypal-YYYY.csv

Please note that when exporting from paypal the format needs to be
"Comma Delimited - All Activity"

Also the following fields need to be enabled in "Customize Download Fields":
"PayPal Website Payments"
    Item ID
"Other Fields"
    Subject
    Note
    Subscription Number
    Payment Type
    Balance Impact
"""

import csv
from datetime import datetime
import logging
from os import path
from decimal import Decimal, InvalidOperation
import chardet

from beancount.core import data
from beancount.core import account
from beancount.core import amount
from beancount.ingest import importer
from beancount.ingest import regression

from ..common import BaseMovement, InternalTransaction
from ..common import Accounts as AccountDeclarations

__author__ = 'Ugo Riboni <nerochiaro@gmail.com>'

HEADER = "Date, Time, Time Zone, Name, Type, Status"
FULL_HEADER = HEADER + ", Subject, Currency, Gross, Fee, Net, Note, " \
         "From Email Address, To Email Address, Transaction ID, " \
         "Payment Type,Item ID, Reference Txn ID, Subscription Number, " \
         "Receipt ID, Balance, Balance Impact"

log = logging.getLogger(__name__)

class Accounts:
    root = "Assets:PayPal"
    bank = "Assets:Bank"
    transit = "Assets:Transit:FromPayPal"
    fees = "Expenses:Fees:PayPal"
    sent_refunds = "Expenses:Refunds"
    received_refunds = "Income:Refunds"
    disputed_out = "Expenses:Disputed:PayPal"
    disputed_in = "Income:Disputed:PayPal"
    unknown_income = "Income:Unknown"
    unknown_expense = "Expenses:Unknown"


def paypal_number(value):
    # The old format uses coma to separate decimals and dot as thousand separator,
    # but the new format does the opposite. Detect and convert accordingly.
    value = str(value)
    if value[-3] == ".":
        return Decimal(value.replace(',', ''))
    else:
        return Decimal(value.replace('.', '').replace(',', '.'))

class PayPalMovement(BaseMovement):
    """Movement object that knows how to create itself
    from lines of a CSV file and provides convenient properties"""

    def __init__(self):
        self.source = "PP"
        self.date = None
        self.time = None
        self.currency = None
        self.amount = Decimal(0)
        self.net = Decimal(0)
        self.fee = Decimal(0)
        self.email = ""
        self.note = ""
        self.is_income = True
        self.item = None
        self.conversion_rate = None
        self.original_value = None
        self.account = None

    def from_csv_row(row):
        movement = PayPalMovement()
        movement.date = datetime.strptime(row['Date'], '%d/%m/%Y').date()
        movement.time = datetime.strptime(row['Time'], '%H:%M:%S').time()
        movement.type = row['Type']

        movement.item = row['Item ID']

        # TODO: when passing on to beancount text which contains quotes,
        #       they are not correctly quoted in the ledger entries resulting
        #       in errors. Report the bug to beancount or fix it and propose fix.
        #       This is only a workaround
        movement.subject = row['Subject'].replace('"', '')
        movement.note = row['Note'].replace('"', '')

        movement.currency = row['Currency']
        try:
            movement.amount = paypal_number(row['Gross'])
            movement.fee = paypal_number(row['Fee'])
            movement.net = paypal_number(row['Net'])
        except InvalidOperation:
            log.error("Skipping row with invalid numbers (gross, fee, net): "
                      "%s %s %s", row['Gross'], row['Fee'], row['Net'])

        movement.transaction = row['Transaction ID']
        movement.reference = row['Reference Txn ID'] or None
        movement.id = 'PP-' + movement.transaction

        movement.is_memo = row['Balance Impact'] == 'Memo'

        if movement.is_memo:
            movement.is_income = movement.amount > 0
        else:
            movement.is_income = row['Balance Impact'] == 'Credit'

        # Store only one email address: the one of the person paying
        # us or being paid by us. We already know the other one anyway
        if movement.is_income:
            movement.email = row['From Email Address']
        else:
            movement.email = row['To Email Address']

        return movement

    def __str__(self):
        return " | ".join([self.id, self.email, self.type, str(self.amount),
                           self.item, self.subject, self.note,
                           self.reference or ""])


class PayPalFile:
    """Class which recognizes the type of file from the name and processes it
    accordingly, returning a sequence of movements.
    """

    FIELD_NAMES = [field.strip() for field in FULL_HEADER.split(",")]

    def __init__(self, root_currency):
        self.movements = []
        self.root_currency = root_currency

    def parse(self, file):
        self.exclude = []
        self.movements = self.create_movements(file.name)
        self.copy_from_memos()
        self.handle_instant_conversions()
        self.handle_multicurrency_movements()
        self.movements = [m for m in self.movements if m not in self.exclude]

    # Read data from the file at path into a list of PayPalMovement instances
    def create_movements(self, path):
        movements = []

        # The new paypal format uses UTF8 files with BOM. The csv reader chokes
        # on them and displays the BOM as part of the first key, unless we
        # explicity tell to open the file as UTF-8.
        with open(path, 'rb') as file:
            raw = file.read(32) # at most 32 bytes are returned
            encoding = chardet.detect(raw)['encoding']

        # The old format uses ASCII with a latin 1 encoding (ISO-8859-1),
        # so if unicode is not detected we fall back to that. Bleh.
        if encoding == "ascii":
            encoding = "ISO-8859-1"

        # register a csv dialect that strips leading whitespace in field names, used by
        # the old paypal format.
        csv.register_dialect('paypal', quotechar='"', skipinitialspace=True, strict=True)
        reader = csv.DictReader(open(path, encoding=encoding), dialect="paypal")

        for i, row in enumerate(reader):
            movements.append(PayPalMovement.from_csv_row(row))
        return movements

    # Copy user-submitted data and user information from memo movements
    # into the movements that refence them, so the information can be used
    # in later stages regardless if we are using the original movement or
    # the memo (since either can be referenced by other movements).
    def copy_from_memos(self):
        for movement in self.movements:
            if movement.is_memo:
                self.exclude.append(movement)
                referents = self.find_referents(movement)
                for referent in referents:
                    referent.note = referent.note or movement.note
                    referent.subject = referent.subject or movement.subject
                    referent.email = referent.email or movement.email

    # Return all movements that refer to the passed movement
    def find_referents(self, movement):
        return [m for m in self.movements
                if m.reference == movement.transaction]

    # Handle instantaneous currency conversions by removing 2 of the 3
    # movements that compose such operations (see the notes at the bottom of
    # this file for more discussion on how these work).
    # All we are interested in is how much money arrives in our root
    # account after the conversion, and the details of why.
    # So we are keeping only the leg of the conversion in our main currency,
    # and adding to it all the information from the other two parts, which
    # then get discarded.
    def handle_instant_conversions(self):
        excluded = []
        for movement in self.movements:
            if movement.id in excluded:
                continue
            if (movement.currency == self.root_currency and
                    movement.type == "Currency Conversion"):
                origin, other = self.find_instant_conversion_parts(movement)
                if origin is None or other is None:
                    continue
                movement.email = movement.email or origin.email
                movement.note = movement.note or origin.note
                movement.subject = movement.subject or origin.subject
                movement.original_value = "{0} {1}".format(-other.amount,
                                                           other.currency)
                excluded.append(other)
                excluded.append(origin)

        self.exclude += excluded

    # Given a movement which is the part of an instant currency conversion
    # in the root currency, return both the other part (in the secondary
    # currency) and the movement that originated the conversion (in this order)
    def find_instant_conversion_parts(self, movement):
        origin = None
        other = None
        for m in self.movements:
            if m.date == movement.date and m.time == movement.time:
                if m == movement:
                    continue
                elif m.type == "Currency Conversion":
                    other = m
                else:
                    if origin is not None:
                        if m.type == "Add Funds from a Bank Account":
                            # When we pay for something and we don't have
                            # enough funds in the account, money will be
                            # pulled from the bank account. This transaction
                            # happens at the same instant as the payment, and
                            # has a reference to the payment.
                            # Currency conversions also follows the same
                            # pattern. So when both a currency conversion and
                            # a transfer from the bank happen together, we need
                            # to explicitly handle this case to prevent False
                            # errors.
                            pass
                        else:
                            logging.error("Instant conversion with more than"
                                          "one origin: %s => %s", m, origin)
                    else:
                        origin = m

        if other is None or origin is None:
            logging.error("Instant conversion missing parts: "
                          "origin: %s other: %s", origin, other)
            return None, None

        return origin, other

    # Handle transfers between accounts in differen currencies.
    # We are moving money from the secondary currency account to the main:
    # there will be 2 movements, one debiting and one crediting, happening
    # exactly on the same instant.
    # We combine these into one single movement which stores the conversion
    # rate (beancount needs it, since it can't figure it out automatically yet)
    # Note that we keep only the leg of the transfer in the secondary currency,
    # since we know what our main currency is but not which secondary currency
    # we are transferring from.
    # NOTE: transfers from the main currency to secondary one are not supported
    def handle_multicurrency_movements(self):
        filtered = []
        for movement in self.movements:
            if movement.type == "Transfer":
                if movement.currency == self.root_currency:
                    continue

                other = self.find_other_transfer(movement)
                if other is None:
                    log.error("Missing other side of Transfer: %s",
                              movement.id)
                    continue

                movement.conversion_rate = other.amount / movement.amount
                movement.conversion_rate = abs(movement.conversion_rate)

            filtered.append(movement)

        self.movements = filtered

    # Given any one leg of a multicurrency transfer, return the other leg
    def find_other_transfer(self, movement):
        results = [m for m in self.movements if (m.date == movement.date and
                                                 m.time == movement.time and
                                                 m.type == m.type and
                                                 m is not movement)]
        return results[0] if len(results) == 1 else None


class Importer(importer.ImporterProtocol):

    def __init__(self, data_root, root_currency, accounts_opening_date):
        self.data_root = data_root
        self.root_currency = root_currency
        self.accounts_opening_date = accounts_opening_date

    def identify(self, file):
        return path.splitext(file.name)[1].lower() == ".csv" and path.basename(file.name).startswith("paypal-")

    def file_name(self, file):
        return path.basename(file.name)

    def file_account(self, _):
        return Accounts.root

    def file_date(self, file):
        name = path.splitext(path.basename(file.name))[0]
        return datetime.strptime(name, 'paypal-%Y').date()

    def extract(self, file):
        paypal_file = PayPalFile(self.root_currency)
        paypal_file.parse(file)

        entries = []
        for movement in paypal_file.movements:
            # The index is not accurate since we combined or deleted movements
            # spanning multiple lines. So just put 0 instead of a bogus value.
            # TODO: keep original source line of at least one of the movements
            meta = data.new_metadata(file.name, 0)
            meta['movement_id'] = movement.id

            transaction = InternalTransaction(movement.date, meta)

            gross = amount.Amount(movement.amount, movement.currency)
            net = amount.Amount(movement.net, movement.currency)
            fee = amount.Amount(movement.fee, movement.currency)

            # If a paypal user complains or reports a movement as invalid,
            # paypal will put it on hold, i.e. remove it from our balance until
            # the dispute is somehow resolved in the account holder favor and
            # the money is returned.
            # Supposedly if the disputed is resolved in the favor of the user,
            # nothing else happens and the money is gone.
            # Either way, the Reference field will keep track of the dispute
            # chain, so let's make sure to keep it around.
            if movement.type == "Temporary Hold":
                transaction.post(Accounts.disputed_out, -net)
                transaction.post(Accounts.root)
                transaction.narration = "Paypal: " + movement.type
                meta['reference'] = 'PP-' + movement.reference
            elif movement.type == "Update to Reversal":
                transaction.post(Accounts.disputed_in, -net)
                transaction.post(Accounts.root)
                transaction.narration = "Paypal: " + movement.type
                meta['reference'] = 'PP-' + movement.reference

            elif movement.is_income:
                if movement.type == "Add Funds from a Bank Account":
                    # TODO: verify that transfers from the bank account are
                    # actually instantaneous, or if we need to create a
                    # transit account for them.
                    meta['reference'] = movement.reference
                    transaction.narration = "Paypal: " + movement.type
                    transaction.post(Accounts.root, net)
                    transaction.post(Accounts.bank, -net)
                else:
                    if movement.account == "Income:Refunds":
                        meta['reference'] = movement.reference
                    else:
                        movement.account = Accounts.unknown_income

                    meta['entity_id'] = 'unknown'
                    transaction.payee = movement.email

                    # Set the narration field to the note and add a subject metadata, if any
                    transaction.narration = movement.note
                    if movement.subject:
                        meta['paypal_subject'] = movement.subject

                    # Movements in different currencies than the root one can only
                    # go into their own sub-accounts.
                    if movement.currency == self.root_currency:
                        target = Accounts.root
                    else:
                        target = account.join(Accounts.root, movement.currency)
                        entries += self.open_account(target)

                    transaction.post(movement.account, -gross)
                    transaction.post(target, net)
                    if movement.fee != 0:
                        transaction.post(Accounts.fees, -fee)
            else:
                if movement.type in ["Withdraw Funds to Bank Account", "General Withdrawal"]:
                    transaction.post(Accounts.root, net)
                    transaction.post(Accounts.transit)
                elif movement.type in ["Refund", "Payment Refund"]:
                    transaction.post(Accounts.sent_refunds, -net)
                    transaction.post(Accounts.root)
                    meta['reference'] = 'PP-' + movement.reference
                    transaction.narration = movement.note
                elif movement.type == "Transfer":
                    # We are moving money from accounts in different
                    # currencies, so there is a currency conversion involved.
                    # We have the source amount and the conversion rate, so we
                    # can calculate the destination amount.
                    source = account.join(Accounts.root, movement.currency)
                    entries += self.open_account(source)

                    converted = movement.amount * movement.conversion_rate
                    converted = amount.Amount(converted, self.root_currency)
                    price = amount.Amount(movement.conversion_rate,
                                          self.root_currency)
                    transaction.post(source, net, price=price)
                    transaction.post(Accounts.root, -converted)
                else:
                    # We actually purchased something or paid for some service
                    meta['entity_id'] = 'unknown'
                    transaction.payee = movement.email
                    transaction.post(Accounts.root, net)
                    transaction.post(Accounts.unknown_expense)

                    # Set the narration field to note and add a piece of metadata
                    # for the subject, if any
                    transaction.narration = movement.note
                    if movement.subject:
                        meta['paypal_subject'] = movement.subject

            if movement.original_value:
                meta['converted_from'] = movement.original_value

            if movement.item:
                meta['product_id'] = movement.item

            entries.append(transaction.to_beancount())

        return entries

    # Return a list of beancount 'open' statements necessary to open an account
    # with the specified name. The list will be empty if the account has been
    # created already in a previous run of the importer within the same session
    def open_account(self, name):
        opening = AccountDeclarations.open(name, self.accounts_opening_date)
        return [opening] if opening is not None else []


def test():
    # Create an importer instance for running the regression tests.
    importer = Importer()
    yield from regression.compare_sample_files(importer, __file__)


"""
=== NOTES ===

The following notes contain some explanation on the automated importer
implemented by this file, by describing how some of the Paypal data streams
in the CSV exports work. This is from direct observation and some information
from various internet sources, but I could not find good documentation directly
from Paypal themselves.

From here on we use the convention where we put the movement Type field
between quotes, then any other fields as (Name: Value).

== Memos ==

Sometimes we receive movement pairs that look like this:

- "Payment Received" (Balance Impact: Memo)
- ... potentially more movements may happen in between ...
- "Update to Payment Received" (Balance Impact: Credit/Debit)

In this case the actual date the money is really present in the account is at
the time of the last movement, but all the information about who paid, notes,
etc. is contained it the Memo movement.

== Currency conversion ==

Currency conversion can work in two ways in paypal:
(a) we have various sub-accounts in different currencies and we manually
    order conversions and transfers between them
(b) we only have one account in our main currency, and any movements in
    different currencies will be converted on the fly

Let's assume our main currency is EUR and we receive USD funds.
In case (a) the stream looks like this:

- "Payment Received" USD
- ... we potentially receive more USD or EUR movements ...
- ... we decide to transfer all or just some of our USD to EUR ...
- "Transfer" (Name: "To Euro") USD
- "Transfer" (Name: "From U.S. Dollar") EUR

The Transfer movements will not reference in any way the various movemements
that contributed to create the USD balance we are transferring (and there is
really no way for this to be possible, especially if the user chose to transfer
less than the full balance, which PayPal allows).

In this case the importer will have movements in the secondary currencies
going to accounts where the last component is CURRENCY (i.e. Assets:PayPal:AUD)
and transfers from these accounts to the main account will appear too.

Instead when we have the account setup as in case (b) we get this stream
(though not necessarily in the same order):

(1) "Payment Received" USD
(2) "Currency Conversion" (Name: "To Euro") USD
(3) "Currency Conversion" (Name: "From U.S. Dollar") EUR

Both the "Currency Conversion" movements have a reference to the
"Payment Received" movement, and they all happen exactly at the same instant.

So we can essentially pretend that the transfer in the secondary currency never
happened and just act as if received the money in EUR in the first place by
keeping the movement (2) and discarding the others (after copying the
necessary data from (1) before it is discarded).
For completeness this importer also adds a "converted-from" metadata retaining
the original value in the secondary currency from (3).
"""
