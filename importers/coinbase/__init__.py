"""
Importer for Coinbase bitcoin processor.

The reconstruct the full history of all our coinbase movements we need to
downlaod the following reports:

EUR Wallet - Transaction History => history-eur.csv
My Wallet - Transaction History => history-btc.csv

My Wallet - Merchant Order History => merchant-main.csv
My Wallet - Merchant Order Mispayments => merchant-mispayments.csv

ClubMate Wallet - Merchant Order History => merchant-clubmate.csv
ClubMate Wallet - Merchant Order Mispayments => merchant-mispayments.csv

In addition to this the prices for bitcoin to EUR conversion need to be
updated too. For our calculations we use the bitcoinaverage service
https://apiv2.bitcoinaverage.com/indices/global/history/BTCEUR?period=alltime&format=json
"""

import csv
from datetime import date, datetime
import logging
from os import path
from decimal import Decimal

from beancount.core import data
from beancount.core import amount
from beancount.ingest import importer

from ..common import BaseMovement, InternalTransaction

__author__ = 'Ugo Riboni <nerochiaro@gmail.com>'

log = logging.getLogger(__name__)

class Accounts:
    root = "Assets:Coinbase:BTC"
    root_eur = "Assets:Coinbase:EUR"
    transit = "Assets:Transit:FromCoinbase"
    cash = "Assets:Cash"
    fees = "Expenses:Fees:Coinbase"
    unknown_income = "Income:Unknown"
    unknown_expense = "Expenses:Unknown"


class CoinbaseFile:
    """This is the base class for all coinbase file readers. Coinbase files
       are not pure CSV, but they have some lines before the CSV header which
       are specific to Coinbase and must be skipped.
    """

    def __init__(self, file, record_class):
        self.file = file
        self.record_class = record_class

    def parse(self):
        # Skip coinbase specific intro lines, until we reach a blank line
        lines = [line.strip() for line in self.file.contents().split("\n")]
        i = 0
        for line in lines:
            i += 1
            if len(line) == 0:
                break

        # Skip one extra line, which is the real CSV header
        fields = lines[i].split(",")
        lines = lines[i + 1:]

        reader = csv.DictReader(lines, fieldnames=fields)
        transactions = []
        for row in reader:
            if len(row) > 0:
                transaction = self.record_class.from_csv_row(row)
                transactions.append(transaction)

        return transactions

# --------------------------------------------------------------------------------------------------------------------------------------------
# --------------------------------------------------------------------------------------------------------------------------------------------
# --------------------------------------------------------------------------------------------------------------------------------------------

class MerchantTransaction():
    """Merchant transaction, which contains the real customer info, product
    info and original amount of the order in Euro"""

    def from_csv_row(row):
        transaction = MerchantTransaction()

        timestamp = datetime.strptime(row['Timestamp'], '%Y-%m-%d %H:%M:%S %z');
        transaction.date = timestamp.date()
        transaction.time = timestamp.time()

        transaction.amount = Decimal(row['BTC Price'])
        transaction.native = Decimal(row['Native Price'])
        transaction.currency = row['Currency']
        transaction.conversion_rate = transaction.native / transaction.amount
        transaction.conversion_rate = abs(transaction.conversion_rate)

        transaction.code = row['Tracking Code']
        transaction.status = row['Status']
        transaction.button_name = row['Button Name']
        transaction.button_code = row['Button Code']
        transaction.email = row['Customer Email']

        transaction.id = 'CB-' + transaction.code.upper()

        return transaction

class MerchantImporter(importer.ImporterProtocol):

    def __init__(self, data_root, currency):
        self.data_root = data_root
        self.currency = currency

    def identify(self, file):
        name = path.basename(file.name).lower()
        return name.startswith("merchant-") and name.endswith(".csv")

    def file_name(self, file):
        return path.basename(file.name)

    def file_account(self, _):
        return Accounts.root

    def extract(self, file):
        movements = CoinbaseFile(file, MerchantTransaction).parse()

        entries = []
        index = 0
        for movement in movements:
            # Skip mispaid orders, they have their own separate report
            if movement.status == "mispaid":
                continue

            meta = data.new_metadata(file.name, index)
            meta['movement_id'] = movement.id

            transaction = InternalTransaction(movement.date, meta)
            value = amount.Amount(movement.native, movement.currency)

            meta['entity_id'] = 'unknown'
            transaction.payee = movement.email
            transaction.narration = movement.button_name
            meta['product_id'] = movement.button_code

            # All merchant payments are immediately converted and go straigth
            # to the EUR account.
            transaction.move(-value, Accounts.unknown_income,
                                     to=Accounts.root_eur)
            meta['converted_from'] = "{0} {1}".format(movement.amount,
                                                      self.currency)

            entries.append(transaction.to_beancount())

        return entries

# --------------------------------------------------------------------------------------------------------------------------------------------
# --------------------------------------------------------------------------------------------------------------------------------------------
# --------------------------------------------------------------------------------------------------------------------------------------------

class HistoryTransaction():
    """History transaction"""

    def from_csv_row(row):
        transaction = HistoryTransaction()

        timestamp = datetime.strptime(row['Timestamp'], '%Y-%m-%d %H:%M:%S %z');
        transaction.date = timestamp.date()
        transaction.time = timestamp.time()

        transaction.balance = Decimal(row['Balance'])
        transaction.amount = Decimal(row['Amount'])

        transaction.native = Decimal(0)
        if len(row['Transfer Total']):
            transaction.native = Decimal(row['Transfer Total'])

        transaction.currency = row['Transfer Total Currency']
        transaction.conversion_rate = transaction.native / transaction.amount
        transaction.conversion_rate = abs(transaction.conversion_rate)

        transaction.fee = Decimal(0)
        if len(row['Transfer Fee']):
            transaction.fee = Decimal(row['Transfer Fee'])

        # There are two types of things that can cause money in BTC to come in
        # or go out of the account: either we do an internal EUR/BTC transfer
        # or we send/receive bitcoins to/from somone.
        # In this report, the only way to figure this out is quite fragile in
        # case we are buying BTC: the Notes field will read
        # "Bought x.xxx BTC for €x.xxx EUR.".
        # In case we are selling BTC, it's a little better, as the "To" field
        # will have the magic value "5083a5425b4a5b0200000001" which possibly
        # represents our EUR account.
        transaction.is_transfer = (row['Notes'].startswith("Bought ") and \
                                   row['Notes'].endswith("EUR.") and \
                                   " BTC for €" in row['Notes']) or \
                                   row['To'] == '5083a5425b4a5b0200000001'

        transaction.is_order = len(row['Order Tracking Code']) > 0
        transaction.notes = row['Notes']
        transaction.payee = row['To']

        # Use this hack to find id field because the field name contains other
        # garbage (a link to a webpage on coinbase)
        id = [item[1] for item in row.items() \
              if item[0].startswith('Coinbase ID')][0].upper()
        transaction.id = 'CB-' + id

        transaction.converted = Decimal(0)
        transaction.converted_rate = Decimal(0)
        transaction.converted_currency = ''
        return transaction


class HistoryImporter(importer.ImporterProtocol):

    def __init__(self, data_root, currency):
        self.data_root = data_root
        self.currency = currency

    def identify(self, file):
        return path.basename(file.name).lower() == "history-btc.csv"

    def file_name(self, file):
        return path.basename(file.name)

    def file_account(self, _):
        return Accounts.root

    def extract(self, file):
        movements = CoinbaseFile(file, HistoryTransaction).parse()

        # There are things that shouldn't be in this report, but they end up
        # here anyway for whatever reason. So we just exclude them.
        # For example:
        # - Transfer requests: they appear in the web interface but not in any
        #   report and don't seem to be of any use. If the transfer actually
        #   happens we have an actual balance-affecting movement in the report.
        # - Merchant payments: these are accounted for already in the merchant
        #   report and go straight to the EUR wallet, so they don't affect
        #   balance
        balance = Decimal(0.0)
        actual = []
        for movement in movements:
            if balance == movement.balance:
                continue
            balance = movement.balance
            actual.append(movement)

        entries = []
        for movement in actual:
            meta = data.new_metadata(file.name, 0)
            meta['movement_id'] = movement.id

            transaction = InternalTransaction(movement.date, meta)
            transaction.narration = movement.notes

            if movement.native != 0:
                meta['converted_from'] = "{0} {1}".format(movement.native,
                                                          movement.currency)

            value = amount.Amount(movement.amount, self.currency)
            if movement.amount < 0:
                if movement.is_transfer:
                    # This is money moving out of the account, towards the EUR
                    # wallet. We need to specify the conversion rate, since the
                    # target is in EUR.
                    transaction.narration = "Transfer: " + transaction.narration
                    price = amount.Amount(abs(movement.native / movement.amount),
                                          movement.currency)
                    out = amount.Amount(movement.native, movement.currency)

                    transaction.post(Accounts.root, value, price=price)
                    transaction.post(Accounts.root_eur, out)

                    #The spreadsheet seems to say there are fees but
                    # they don't really get applied to the total transferred,
                    # so we ignore them for now.
                    # if movement.fee != 0:
                    #     transaction.post(Accounts.fees, movement.fee)

                else:
                    # we are actually paying someone in BTC.
                    meta['entity_id'] = 'unknown'
                    transaction.payee = movement.payee
                    transaction.move(value, Accounts.root,
                                            to=Accounts.unknown_expense)
            else:
                if movement.is_transfer:
                    # We are buying bitcoins with money from the EUR wallet.
                    transaction.narration = "Transfer: " + transaction.narration
                    price = amount.Amount(abs(movement.native / movement.amount),
                                          movement.currency)
                    out = amount.Amount(movement.native, movement.currency)

                    transaction.post(Accounts.root_eur, -out)
                    transaction.post(Accounts.root, value, price=price)
                    # Again there seem to be fees attached to this but they are
                    # not really applied from what we can see in the reports.
                    # if movement.fee != 0:
                    #     transaction.post(Accounts.fees, movement.fee)
                else:
                    # We are actually receiving a payment in BTC.
                    # This is "fun" because except from the notes we have no
                    # way to identify who's sending us money and why, which
                    # basically means we have to manually categorize this stuff
                    # later on.
                    meta['entity_id'] = 'unknown'
                    transaction.move(-value, Accounts.unknown_income,
                                             to=Accounts.root)

            entries.append(transaction.to_beancount())

        return entries

# --------------------------------------------------------------------------------------------------------------------------------------------
# --------------------------------------------------------------------------------------------------------------------------------------------
# --------------------------------------------------------------------------------------------------------------------------------------------

class EuroTransaction():
    """Euro transaction"""

    def from_csv_row(row):
        transaction = EuroTransaction()

        timestamp = datetime.strptime(row['Timestamp'], '%Y-%m-%d %H:%M:%S %z');
        transaction.date = timestamp.date()
        transaction.time = timestamp.time()

        transaction.amount = Decimal(row['Amount'])
        transaction.currency = row['Currency']
        transaction.net = Decimal(row['Transfer Total'])
        transaction.fee = abs(transaction.amount) - transaction.net

        transaction.notes = row['Notes']
        transaction.type = transaction.notes.split(" ")[0].lower()

        # Use this hack to find id field because the field name contains other
        # garbage (a link to a webpage on coinbase)
        id = [item[1] for item in row.items() \
              if item[0].startswith('Coinbase ID')][0].upper()
        transaction.id = 'CB-' + id

        return transaction


class EuroImporter(importer.ImporterProtocol):

    def __init__(self, data_root):
        self.data_root = data_root

    def identify(self, file):
        return path.basename(file.name).lower() == "history-eur.csv"

    def file_name(self, file):
        return path.basename(file.name)

    def file_account(self, _):
        return Accounts.root_eur

    def extract(self, file):
        movements = CoinbaseFile(file, EuroTransaction).parse()

        entries = []
        for movement in movements:
            # We are only interested in withdrawals and deposits here, and
            # associated fees.
            # All other types of movements are accounted for by the rest of the
            # coinbase report files already.
            if movement.type != "withdrew" and movement.type != "deposited":
                continue

            meta = data.new_metadata(file.name, 0)
            meta['movement_id'] = movement.id

            transaction = InternalTransaction(movement.date, meta)
            transaction.narration = "Transfer: " + movement.notes

            value = amount.Amount(movement.amount, movement.currency)
            net = amount.Amount(movement.net, movement.currency)
            if movement.amount > 0:
                transaction.move(-value, Accounts.transit,
                                 to=Accounts.root_eur)
            else:
                fee = amount.Amount(movement.fee, movement.currency)
                transaction.post(Accounts.root_eur, value)
                transaction.post(Accounts.transit, net)
                if (movement.fee > 0):
                    transaction.post(Accounts.fees, fee)

            entries.append(transaction.to_beancount())

        return entries

# --------------------------------------------------------------------------------------------------------------------------------------------
# --------------------------------------------------------------------------------------------------------------------------------------------
# --------------------------------------------------------------------------------------------------------------------------------------------

class MispayTransaction():
    """Describes a mispayment: a merchant transaction for which the wrong
       amount was sent, and then was fixed with the proper amount."""

    def from_csv_row(row):
        transaction = MispayTransaction()

        timestamp = datetime.strptime(row['Timestamp'], '%Y-%m-%d %H:%M:%S %z');
        transaction.date = timestamp.date()
        transaction.time = timestamp.time()

        transaction.amount = Decimal(row['Native Amount'])
        transaction.currency = row['Currency']
        transaction.native = Decimal(row['BTC Amount'])
        transaction.conversion_rate = transaction.native / transaction.amount
        transaction.conversion_rate = abs(transaction.conversion_rate)

        transaction.button_name = row['Button Name']
        transaction.button_code = row['Button Code']
        transaction.original_order = row['Order Tracking Code']

        transaction.id = 'CB-' + row['Tracking Code'].upper()

        return transaction


class MispayImporter(importer.ImporterProtocol):

    def __init__(self, data_root, currency):
        self.data_root = data_root
        self.currency = currency

    def identify(self, file):
        name = path.basename(file.name).lower()
        return name.startswith("mispayments-") and name.endswith(".csv")

    def file_name(self, file):
        return path.basename(file.name)

    def file_account(self, _):
        return Accounts.root

    def extract(self, file):
        movements = CoinbaseFile(file, MispayTransaction).parse()

        entries = []
        for movement in movements:
            meta = data.new_metadata(file.name, 0)
            meta['movement_id'] = movement.id

            transaction = InternalTransaction(movement.date, meta)
            value = amount.Amount(movement.amount, movement.currency)

            meta['entity_id'] = 'unknown'
            transaction.narration = movement.button_name
            meta['reference'] = 'CB-' + movement.original_order
            meta['product_id'] = movement.button_code

            # The actual payments for a mispaid order are immediately converted
            # and go straigth to the EUR account.
            transaction.move(-value, Accounts.unknown_income,
                                     to=Accounts.root_eur)
            meta['converted_from'] = "{0} {1}".format(movement.native,
                                                      self.currency)

            entries.append(transaction.to_beancount())

        return entries
