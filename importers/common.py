from beancount.core import data
from beancount.core import account


class BaseMovement:
    def __init__(self):
        self.source = None
        self.id = None


class InternalPosting:
    def __init__(self, account, units=None, price=None, meta=None):
        self.account = account
        self.units = units
        self.meta = meta
        self.price = price

    def to_beancount(self):
        return data.Posting(self.account, self.units, None, self.price, None,
                            self.meta)


class InternalTransaction:
    """Since the Transaction and Posting objects from beancount.core.data
    can only be created by passing all arguments, and then the properties are
    read-only, it does not give us the flexibility we need when working in
    importers.
    Therefore we use this more convenient internal class and generate a
    beancount Transaction at the end of the process"""

    def __init__(self, date, meta=None, payee=None, narration=None):
        self.date = date
        self.payee = payee
        self.narration = narration
        self.meta = meta
        self.postings = []
        self.flag = "*"
        self.tags = set()
        self.links = set()

    def post(self, account, units=None, price=None):
        self.postings.append(InternalPosting(account, units, price=price))

    def move(self, value, src, to):
        """Shorthand for simple cases when we move a value from an account to
        another. The value in the target account is left implicit."""
        self.postings.append(InternalPosting(src, value))
        self.postings.append(InternalPosting(to))

    def to_beancount(self):
        # Remove any quotes from user-submitted strings as no matter how we
        # quote them they end up being messed up at some stage later in the pipeline
        # when run through bean-* tools.
        # TODO: investigate and file a bug report
        payee = (self.payee or '').replace('"', '')
        narration = (self.narration or '').replace('"', '')

        postings = [posting.to_beancount() for posting in self.postings]
        return data.Transaction(self.meta, self.date, self.flag,
                                payee, narration,
                                self.tags, self.links,
                                postings)


# This class allows creating declarations for opening new accounts.
# It also keeps track of all accounts that it has opened so far, to avoid
# creating duplicates, which are not legal in beancount.
class Accounts:
    opened = []

    @staticmethod
    def open(name, when, currency=None):
        if currency is None:
            currencies = [account.leaf(name)]
        else:
            currencies = [currency]
        meta = data.new_metadata(__name__, 0)
        statement = data.Open(meta, when, name, currencies, None)
        for other in Accounts.opened:
            if other == statement:
                return None
        Accounts.opened.append(statement)
        return statement
