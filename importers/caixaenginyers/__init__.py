"""
Importer for Caixa Enginyers bank data.

We store one file in Q43 format per year, named bank-YYYY.Q43.
This has two exceptions:
- The first few months of 2013 are in bank-2013-start.csv due to the bank
  not being able to provide old movements in Q43 format
- The current year is named bank-current.Q43 and is periodically overwritten
  with fresh information we rename it at the end of the year.

Q43 files are following the Spanish Norma43 format, defined here:
https://empresas.bankinter.com/stf/plataformas/empresas/gestion/ficheros/formatos_fichero/norma_43_ingles.pdf

It is an annoying format with no fixed ids on the single movements, so we have
to synthetize the IDs by concatenating the date and a progressive number for
days with more than one movement (the movements in the files are always in
chronological order).
We also know that no information for a single date is ever split over separate
files.

In the case of CSV files we assume all movements are in the root currency that
we pass in from the configuration (since we create and control these files).
For Q43 files, we check that the root currency matches the file's declared
currency, and report an error if it does not.
"""

import csv
from datetime import date, datetime
import logging
from os import path
from decimal import Decimal

from beancount.core import data
from beancount.core import amount
from beancount.ingest import importer
from beancount.ingest import regression

from ..common import BaseMovement, InternalTransaction

__author__ = 'Ugo Riboni <nerochiaro@gmail.com>'

header = "DATA D'OPERACIÓ,CONCEPTE,DATA VALOR,IMPORT,SALDO"
fieldnames = ['DATE', 'DESCRIPTION', 'VALUEDATE', 'AMOUNT', 'BALANCE']

transfers = {
  "TRANSF CTE DE:PayPal Europe S.a": "Assets:Transit:FromPayPal",
  "TRANSF CTE DE:PayPal (Europe) S": "Assets:Transit:FromPayPal",
  "TRANSF CTE DE:Coinbase UK, Ltd": "Assets:Transit:FromCoinbase",
  "TRANSFERENCIA A: Coinbase UK, Ltd.": "Assets:Transit:FromCoinbase",
  "INGRES EFECTIU": "Assets:Cash",
  "INGR.EFECTIVO": "Assets:Cash",
}

typemap = {
  "TRANSF CTE DE:": "Payment",
  "TRANSF CTA DE:": "Payment",
  "TRASPAS A COMPTE DE:": "Payment",
  "TRASPASO A CTA DE:": "Payment",
  "ABONAMENT TRANSFERENCIA EUR NO RESIDEN": "Payment",
  "TRANSFERENCIA A:": "Out",
  "TRANSF INMEDIATA A:": "Out",
  "R/ ": "Out",
  "XEC BANCARI ": "Out",
  "TARGETA *2033 ": "Out", # debit card
  "TARJETA *2033 ": "Out", # debit card
  "COMISSIÓ": "Fee",
  "COMISSIO": "Fee",
  "COMISIÓN MANTENIMIENTO": "Fee",
  "COMISION PRESENTACION TRANSFERENCIAS": "Fee",
  "COMISION TRANSF. INMEDIATA": "Fee",
  "DEBIT TITOL COOPERATIVA": "Fee",
  "COM.": "Fee",
  "PAGAMENT HISENDA": "Tax",
  "LIQUIDACIO TARGETA": "CreditCard",
  "ANUL.INGRES EFECTIU": "Cancel",
  "DEVOLUCIO TRANSFERENCIA": "Refund",
  "ABONAMENT SOL DEV TRANSF": "Refund",
  "DEBIT RECHAZO SOL DEV TRANSF": "Cancel",
}

log = logging.getLogger(__name__)


class Accounts:
    root = "Assets:Bank"
    cash = "Assets:Cash"
    fees = "Expenses:Fees:Bank"
    taxes = "Expenses:Taxes"
    credit = "Liabilities:CreditCard"
    refunds = "Expenses:Refunds"
    refunds_in = "Income:Refunds"
    unknown_income = "Income:Unknown"
    unknown_expense = "Expenses:Unknown"

class IdGenerator:
    """The bank files do not provide unique IDs in their lines. Therefore we
    create them by using a combination of date and a progressive number for
    movements that happened on the same date (assuming they are in
    chronological order in the file)"""

    def __init__(self):
        self.date = None
        self.prog = None

    def generate(self, date):
        if self.date != date:
            self.date = date
            self.prog = 0
        else:
            self.prog += 1

        return 'BK-' + self.date.strftime('%Y%m%d') + '-' + str(self.prog)


class BankMovement(BaseMovement):
    """Common bank movement object that knows how to create itself
    from lines of a CSV or Q43 files and provides convenient properties"""

    def __init__(self):
        self.source = "BK"
        self.date = None
        self.amount = Decimal(0)
        self.description = ""

    def from_csv_row(row):
        movement = BankMovement()
        movement.date = datetime.strptime(row['DATE'], '%d/%m/%Y').date()
        movement.amount = Decimal(row['AMOUNT'].replace(',', '.'))
        movement.description = row['DESCRIPTION']
        return movement

    def from_q43_row(row):
        movement = BankMovement()
        movement.date = date(int("20" + row[10:12]),
                             int(row[12:14]), int(row[14:16]))
        movement.amount = Decimal(row[28:42]) / 100
        if row[27:28] == "1":
            movement.amount = -movement.amount
        return movement


class BankFile:
    """Class which recognizes the type of file from the name and processes it
    accordingly, returning a sequence of BankMovements"""

    def __init__(self, file):
        self.ids = IdGenerator()
        self.file = file

    def parse(self, root_currency_code):
        if path.splitext(self.file.name)[1].lower() == ".csv":
            return self.parse_csv()
        else:
            return self.parse_norma_43(root_currency_code)

    def parse_csv(self):
        reader = csv.DictReader(open(self.file.name), fieldnames=fieldnames)
        movements = []
        for i, row in enumerate(reader):
            # Skip header. Please note that the header is necessary as we use
            # it to identify if the file is a bank csv file or a csv file from
            # some other source
            if i == 0:
                continue

            movement = BankMovement.from_csv_row(row)
            movement.id = self.ids.generate(movement.date)
            movements.append(movement)

        return movements

    def parse_norma_43(self, root_currency_code):
        movements = []
        with open(self.file.name, encoding='iso8859-1') as f:
            for row in f:
                kind = int(row[0:2])
                if kind == 11: # account header record
                    # Report an error if the currency code from our
                    # configuration does not match the account's currency
                    currency_code = int(row[47:50])
                    if currency_code != root_currency_code:
                        logging.error("Account currency code %s does not "
                                      "match specified root currency code %s",
                                      root_currency_code, currency_code)
                elif kind == 22: # movement record
                    movement = BankMovement.from_q43_row(row)
                    movement.id = self.ids.generate(movement.date)
                    movements.append(movement)
                elif kind == 23: # movement details record (optional)
                    movements[-1].description = row[4:42].strip()
                # TODO: extact initial balance from header row (11) and
                #       insert opening balance statement
                # TODO: extact closing balance from footer row (33) and
                #       insert closing balance statement

        return movements


class Importer(importer.ImporterProtocol):

    def __init__(self, data_root, root_currency, root_currency_code):
        self.currency = root_currency
        self.currency_code = root_currency_code
        self.data_root = data_root

    def identify(self, file):
        if path.splitext(file.name)[1].lower() == ".csv":
            head = file.contents().split("\n")[0]
            return header == head
        else:
            return path.splitext(file.name)[1].lower() == ".q43"

    def file_name(self, file):
        return path.basename(file.name)

    def file_account(self, _):
        return Accounts.root

    def file_date(self, file):
        name = path.splitext(path.basename(file.name))[0]
        return datetime.strptime(name, 'bank-%Y').date()

    def extract(self, file):
        movements = BankFile(file).parse(self.currency_code)

        entries = []
        index = 0
        for index, movement in enumerate(movements):
            meta = data.new_metadata(file.name, index)
            meta['movement_id'] = movement.id

            transaction = InternalTransaction(movement.date, meta)
            value = amount.Amount(movement.amount, self.currency)

            # Decide type of movement by checking if description starts with
            # specific strings, and if it does parse the rest of the
            # description to add more information to the transaction.
            rtype = None
            source = None
            alias = ''
            for i, stamp in enumerate(transfers):
                if movement.description.startswith(stamp):
                    rtype = "Transfer"
                    source = transfers[stamp]
            if rtype is None:
                for i, stamp in enumerate(typemap):
                    if movement.description.startswith(stamp):
                        rtype = typemap[stamp]
                        alias = movement.description[len(stamp):].strip()
                        break

            if rtype == 'Payment':
                meta['entity_id'] = 'unknown'
                transaction.payee = alias
                transaction.narration = movement.description
                transaction.move(-value, Accounts.unknown_income,
                                         to=Accounts.root)
            elif rtype == 'Fee':
                # TODO: link fee to related movement, if any
                # (only for movements from q43 files)
                transaction.payee = "Caixa Enginyers"
                transaction.narration = movement.description
                transaction.move(value, Accounts.root, to=Accounts.fees)
            elif rtype == 'Transfer':
                transaction.narration = "Transfer between accounts"
                transaction.move(-value, source, to=Accounts.root)
            elif rtype == 'Out':
                meta['entity_id'] = 'unknown'
                transaction.payee = alias
                transaction.narration = movement.description
                transaction.move(value, Accounts.root,
                                        to=Accounts.unknown_expense)
            elif rtype == 'Tax':
                transaction.narration = "Taxes"
                transaction.move(value, Accounts.root, to=Accounts.taxes)
            elif rtype == 'CreditCard':
                cardtype, digits, when = alias.strip().split(" ")
                when = datetime.strptime(when, "%m/%y").date()
                transaction.narration = "Refund credit card {0} {1} until " \
                                        "{2}".format(cardtype, digits,
                                                     when.strftime("%b %Y"))
                transaction.move(value, Accounts.root, to=Accounts.credit)
            elif rtype == 'Cancel':
                transaction.narration = "Cancelled Deposit"
                transaction.move(value, Accounts.root, to=Accounts.refunds)
            elif rtype == 'Refund':
                transaction.narration = "Refunded Transaction"
                transaction.move(value, Accounts.refunds_in, to=Accounts.root)
            else:
                log.error("Skipping unknown row type: %s; \"%s\" %s %s",
                          rtype, movement.description, value, movement.id)
                continue

            entries.append(transaction.to_beancount())

        return entries


def test():
    # Create an importer instance for running the regression tests.
    importer = Importer()
    yield from regression.compare_sample_files(importer, __file__)
