"""
Importer for the manual cash register file.

We keep a register for money paid in cash in a simple spreadsheet file.

People place envelopes with money (payments) and receipts (expenses)
in a safe box, and periodically the box gets opened, all the contents
logged and taken to the bank.

Because of this we only approximately know when each single movement was
made and they all appear in the log clustered around the time when we open
the box and do the logging.
"""

import csv
from datetime import datetime
import logging
from os import path
from decimal import Decimal, InvalidOperation

from beancount.core import data
from beancount.core import account
from beancount.core import amount
from beancount.ingest import importer

from ..common import BaseMovement, InternalTransaction

__author__ = 'Ugo Riboni <nerochiaro@gmail.com>'

HEADER = "ID,Deposited,Expected,Date of Deposit,Date of Payment,Amount,Type,Months,Year,Member,Customer,Invoice,VAT,Notes"

log = logging.getLogger(__name__)


class Accounts:
    root = "Assets:Cash"
    unknown_income = "Income:Unknown"
    unknown_expense = "Expenses:Unknown"


# def paypal_number(value):
#     return Decimal(str(value).replace('.', '').replace(',', '.'))

month_names = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
               "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"]
def parse_date(text_date):
    """Manually parse the date from the spreadsheet because
    we always use months abbreviations in English, and strptime's
    %b operator is locale-sensitive instead"""
    day, month_name, year = text_date.split(" ")
    month = month_names.index(month_name)
    return datetime(int(year), month + 1,int(day)).date()

class CashMovement(BaseMovement):
    """Movement object that knows how to create itself
    from lines of a CSV file and provides convenient properties"""

    def __init__(self):
        self.source = "CA"
        self.date = None
        self.amount = Decimal(0)
        self.is_income = True
        self.payee = ""
        self.narration = ""
        self.type = None
        self.months = []
        self.invoice = None
        self.VAT = None

    def from_csv_row(row):
        # TODO: more sanity checks and output errors if bad data
        movement = CashMovement()
        movement.id = "CA-" + row['ID'].zfill(6)
        date = row['Date of Payment'].strip() or row['Date of Deposit'].strip()
        movement.date = parse_date(date)

        movement.amount = Decimal(row['Amount'].replace("€ ", ""))
        movement.is_income = movement.amount > 0
        movement.type = row['Type'].strip()
        movement.narration = row['Notes'].strip()

        if movement.is_income:
            movement.payee = row['Member'].strip()
        else:
            movement.payee = row['Customer'].strip()
            movement.VAT = row['VAT'].strip()
            movement.invoice = row['Invoice'].strip()

        movement.parse_months(row['Months'], row['Year'])

        return movement

    def parse_months(self, months, year):
        # TODO: more sanity checks and output errors if bad data
        if year.strip() and months.strip():
            year = int(year.strip())
            self.months = [datetime(year, int(m), 1)
                           for m in months.strip().split(",")]
        else:
            # If there is no metadata about the payment period then use
            # the date of deposit as the payment period
            self.months = [datetime(self.date.year, self.date.month, self.date.day)]

    def __str__(self):
        return " | ".join([self.id, self.date, self.account, str(self.amount),
                           self.payee, self.narration])

def month_to_meta(month):
    return "-".join([str(month.date().year), "{:0>2}".format(month.date().month)])

class CashFile:
    """Class which recognizes the type of file from the name and processes it
    accordingly, returning a sequence of movements.
    """

    FIELD_NAMES = [field.strip() for field in HEADER.split(",")]

    def __init__(self):
        self.movements = []
        self.name = ""

    def parse(self, file):
        self.name = file.name
        self.movements = self.create_movements(file.name)

    # Read data from the file at path into a list of movement instances
    def create_movements(self, path):
        movements = []
        reader = csv.DictReader(open(path, encoding="UTF-8"),
                                fieldnames=CashFile.FIELD_NAMES)
        for i, row in enumerate(reader):
            # Skip header. Please note that the header is necessary as we use
            # it to identify if the file comes from this source or another
            if i > 0:
                movements.append(CashMovement.from_csv_row(row))
        return movements


class Importer(importer.ImporterProtocol):

    def __init__(self, data_root):
        self.data_root = data_root

    def identify(self, file):
        if path.splitext(file.name)[1].lower() == ".csv":
            head = file.contents().split("\n")[0]
            return head == HEADER
        else:
            return False

    def file_name(self, file):
        return path.basename(file.name)

    def file_account(self, _):
        return Accounts.root

    def extract(self, file):
        cash_file = CashFile()
        cash_file.parse(file)

        entries = []
        for i, movement in enumerate(cash_file.movements):
            for i, month in enumerate(movement.months):
                 # add 1 to the line since we removed the header
                meta = data.new_metadata(file.name, i + 1)
                meta['payment_period'] = month_to_meta(month)

                # In cases where there is multiple months in a single line,
                # each months gets its own transaction in the ledger, with
                # an id that's a progressive based on the original id.
                if len(movement.months) < 2:
                    meta['movement_id'] = movement.id
                else:
                    meta['movement_id'] = "-".join([str(movement.id),
                                                    "{:0>2}".format(i + 1)])

                meta['entity_id'] = 'unknown'
                if movement.type:
                    meta['payment_type'] = movement.type

                if movement.invoice:
                    meta['payment_invoice_num'] = movement.invoice
                    meta['payment_invoice_vat'] = movement.VAT

                # Keep the deposit date as the date of the transaction, as
                # it matches the order of the lines in the cash CSV file, and
                # we use this date for tax reporting purposes as well.
                transaction = InternalTransaction(movement.date, meta)
                transaction.payee = movement.payee
                transaction.narration = movement.narration

                account = "Income:Unknown" if movement.is_income else "Expenses:Unknown"
                # Divide the amount by the number of months it's paying for,
                # assuming for now that the user had the same membership level
                # during these months.
                value = amount.Amount(-movement.amount / len(movement.months), "EUR")
                transaction.move(value, account, to=Accounts.root)

                entries.append(transaction.to_beancount())

        return entries
